/**
* Commons.h
*/

#include <stdbool.h>
/**
*This fucntion is used to check if a file exists;
* Takes in a filename.
*/

bool file_exists(char *filename);

/*
* This function is used to check if a directory exists
* takes in a directory name
*/
bool directory_exists (char *dirname);