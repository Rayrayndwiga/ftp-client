#include <stdio.h>
#include  <stdlib.h>
#include  <sys/types.h>        
#include  <sys/socket.h>
#include  <netinet/in.h>       				/* struct sockaddr_in, htons, htonl */
#include  <netdb.h>            				/* struct hostent, gethostbyname() */ 
#include  <string.h>
#include  "stream.h"           				/* MAX_BLOCK_SIZE, readn(), writen() */
#include  <ctype.h>
#include  <unistd.h>						/* close() */
#include  <errno.h>
#include  <dirent.h>
#include  <fcntl.h>
#include  "common.h"						/* file_exists(), directory_exists() */

#define BUFSIZE					512			/* Buf size for array */
#define SERV_TCP_PORT  			40190 		/* default server listening port */
#define SERVICESUPPORTED		50			/* Service requested by client is supported */
#define SERVICENOTSUPPORTED		75			/* Service requested by client is not supported */
#define STARTTRANSFER			90			/* Start transfer of file */
#define ENDTRANSFER				95			/* End of file transfer */
#define SENDFILE				100			/* Opcode for sending a file from the server to the client */
#define GETFILE					200			/* Opcode for getting a file from the client to the server */
#define GOODFILE				300			/* Opcode to show file requested can be sent. */
#define GOODDIR					350			/* Opcode to show there is a directory existing with the name given. */
#define NOFILE					400			/* Opcode to show there isnt any file existing with the name given. */
#define NODIRECTORY				450			/* Opcode to show there isnt any directory existing with the name given. */
#define CLASHFILE				500			/* Opcode to show the file being sent already exists. */
#define BADFILE					600			/* Opcode to show file cannot be accepted cause of other reasons. */
#define ACKN					5			/* Opcode to show acknowledgment was received. */
#define NACKN					6			/* Opcode to show acknowledgement was not received. */
#define MAX_SEND_SIZE 			(1024*5) 	/* Maximum size of data that can be sent in one go */
#define SERVERPWD				700			/* Opcode to find out the current directory of the server */
#define SERVERDIR				750			/* Opcode to list the contents of the servers current directory */
#define SERVERCD				800			/* Opcode to change directory in the server */



/* 	This function prompts the user to input a string 
 *	It stores this string in array passed from the main function
 *	The parameter fname[] is the array that contains the filename that the user has input
 *	The parameter SIZE is the size of the fname array
 */
void getString(char fname[],int SIZE,char option)
{
	int i;
	
	if (option == 'a'){
		printf("Please enter the filename of the file you want to request from the server ?");
	}
	else{
		printf("Please enter the filename of the file you want to send to the server ?");
	}
	
	fgets(fname,SIZE,stdin);
	
	fname[strlen(fname)-1]='\0';
	
}

/*
*This function is used to get the command option of the user 
* by choosing any of the options given in the prompt
*/

void getCommand(char choice[],int SIZE)
{
	
	fgets(choice,SIZE,stdin);
	
	choice[strlen(choice)-1]='\0';
	
}

/**
* This function is used to check the filename 
* that is entered by the user that it is of only one filename 
* located in a directory.
*/
int check_filename(char *filename,int SIZE)
{
	int error = -1;
	char tmp_fname[SIZE];
	char *split_fname;
	int fail = 0;
	
	
	tmp_fname[0] = '\0';
	strcpy(tmp_fname, filename);
	int i;
	
	for (i = 0; i < strlen(tmp_fname) ;i++){
		if(tmp_fname[i] == '/'){
			fail++;
		}
	}

	if ( fail == 0){
		error = 2;
	}
	
	return error;
}

/**
* This fucntion is used to get a command chosen by the user from the prompt
* The users choice is stored in the char array passed into the function.
*/
int getOption(char string[], int SIZE)
{
	char choice[SIZE];
	char tmp_choice[SIZE];
	char *split_choice;
	char *command_one;
	char *command_two;
	int args;
	bool fail;
	do
	{	
		fail = true;
		args = 0;
		choice[0] = '\0';
		tmp_choice[0] = '\0';
		split_choice = NULL; 
		command_one = NULL;
		command_two = NULL;
		
		printf("Which service would you want to use? Choose one from the following commands.\n");
		printf("\t\t pwd: \n");
		printf("\t\t lpwd: \n");
		printf("\t\t dir: \n");
		printf("\t\t ldir: \n");
		printf("\t\t cd: directory\n");
		printf("\t\t lcd: directory\n");
		printf("\t\t get: filename. \n");
		printf("\t\t put: filename. \n");
		printf("\t\t quit: \n");
		
		getCommand(choice,sizeof(choice));
		strcpy(tmp_choice,choice);
		split_choice = strtok(tmp_choice, " ");
		
		while(split_choice != NULL){
			args++;
			if(args == 1){
				command_one = split_choice;
			} 
			else if (args == 2){
				command_two = split_choice;
			}
			split_choice = strtok(NULL, " ");
			
		}
		
		if (args == 2){
			if(strcmp(command_one,"cd") != 0 && strcmp(command_one,"lcd") != 0 && strcmp(command_one,"get") != 0 && strcmp(command_one,"put") != 0){
				printf("please enter a valid input \n");
				fail = false;
			}
			
			if (strcmp(command_one,"get") == 0 || strcmp(command_one,"put") == 0){
				if (check_filename(command_two, SIZE) < 0){
					printf("Please enter a valid filename.\n");
					fail = false;
				}
			}
		}
		else if (args == 1){
			
			if (strcmp(choice,"pwd") != 0 && strcmp(choice,"lpwd") != 0 && strcmp(choice,"dir") != 0 && strcmp(choice,"ldir") != 0 && strcmp(choice,"quit") != 0){
				printf("please enter a valid input \n");
				fail = false;
			}
		}
		else{
			printf("You can only enter a maximum of two arguments for some commands like (cd, lcd, get, put)  \n");
			fail = false;
		}
		
	}while(fail == false);
	
	strcpy(string,choice);
	return args;
}
/**
* This function is used to split a command into two parts.
* That is if the command has two arguments.
* The position chosen chosen represents the part of the command that will be 
* copied into the char array passed into the function.
*/
char* splitGetCmd(char choice[],int SIZE, int pos)
{
	char *command;
	char tmpChoice[SIZE];
	char *command_one;
	char *command_two;
	char *split_choice;
	int args = 0;
	
	command = NULL;
	command_one = NULL;
	command_two = NULL;
	split_choice = NULL;
	tmpChoice[0] = '\0';
	strcpy(tmpChoice,choice);
	
	split_choice = strtok(tmpChoice ," ");
	
	while (split_choice != NULL){
		args++;
		
		if(args == 1){
			command_one = split_choice;
		} 
		else if (args == 2){
			command_two = split_choice;
		}
		split_choice = strtok(NULL, " ");
	}
	
	if (pos == 1){ // get first part of command
		command = command_one;
	}
	else if (pos == 2){ // get second part of command
		command = command_two;
	}
	
	return command;
}
/*
*This function is used to return a character 
* representing a command chosen from the prompt by a user.
*/
char switchOptions(char choice[],int SIZE,int args)
{
	char *command;
	char option = '\0';
	
	if (args == 1){
		if (strcmp(choice,"pwd") == 0)
			option = 'a';
		else if (strcmp(choice,"lpwd") == 0){
			option = 'b';
		}
		else if (strcmp(choice,"dir") == 0){
			option = 'c';
		}
		else if (strcmp(choice,"ldir") == 0){
			option = 'd';
		}
		else if (strcmp(choice,"quit") == 0){
			option = 'q';
		}
	}
	else if (args == 2){
		command = splitGetCmd(choice,SIZE,1); // get first part of command
		if (strcmp(command, "cd") == 0){
			option = 'e';
		}
		else if (strcmp(command, "lcd") == 0){
			option = 'f';
		}
		else if (strcmp(command, "get") == 0){
			option = 'g';
		}
		else if (strcmp(command, "put") == 0){
			option = 'h';
		}
	}
	
	return option;
}

/*
*	This fucntion is used to get the current directory in the server.
*  	socket is the socket used in the communication.
*/
void pwd(int socket)
{
	char dir[MAX_SEND_SIZE];
	char *current;
	int service_command,service_status;
	
	
	service_command = SERVERPWD;
	service_command = htons(service_command);
	
	if ((write(socket, (char *) &service_command, sizeof(service_command))) <= 0 ){
		printf("Client: Can't write service to server.\n");
	}
	/* Get server response if service is supported */
	if ( read(socket, (char *) &service_status, sizeof(service_status)) <= 0 ){
		printf("Client: can't read server service response.\n");
	}
	service_status = ntohs(service_status);
	if ( service_status == SERVICESUPPORTED ){
		printf("Client: service is supported.\n");
	}
	else if ( service_status == SERVICENOTSUPPORTED ){
		printf("Client: service is not supported.\n");
	}
	
	
	/*Get server current directory*/
	
	if ( readn(socket, dir, MAX_SEND_SIZE) <= 0 ){
		printf("Client: error reading server's current directory. \n");
	}
	printf("Current server directory is: \n%s \n",dir);
	
}
/*
* 	This function is used to return the current directory of the client.
*/
void lpwd()
{
	char dir[MAX_SEND_SIZE];
	printf("Client: Current directory is : \n");
	
	if (getcwd(dir, sizeof(dir)) != NULL ){
		printf("%s \n",dir);
	}
	printf("\n\n");
	
}

/*
*	This function is used to list the files and directories in the current directory in the server.
*	socket is the socket used in the communication.
*/
void dir(int socket)
{
	int service_command,service_status, contents, no_read;
	char dir[MAX_SEND_SIZE];
	
	service_command = SERVERDIR;
	service_command = htons(service_command);
	
	if ((write(socket, (char *) &service_command, sizeof(service_command))) <= 0 ){
		printf("Client: Can't write service to server.\n");
	}
	/* Get server response if service is supported */
	if ( read(socket, (char *) &service_status, sizeof(service_status)) <= 0 ){
		printf("Client: can't read server service response.\n");
	}
	service_status = ntohs(service_status);
	if ( service_status == SERVICESUPPORTED ){
		printf("Client: service is supported.\n");
	}
	else if ( service_status == SERVICENOTSUPPORTED ){
		printf("Client: service is not supported.\n");
	}
	contents = 0;
	/* Get number of files in the directory */
	if (read(socket, (char *) &contents, sizeof(contents)) <= 0 ){
		printf("Client:error reading number of contents of directory.");
	}
	contents = ntohs(contents);
	/* Get contents of directory */
	no_read = 0;
	printf("\n\n");
	while ( no_read != contents ){
		dir[0] = '\0';
		if( readn(socket, dir , MAX_SEND_SIZE) <= 0 ){
			printf("Client:error reading file/directory name.");
		}
		printf("%s \n",dir);
		no_read++;
	}
	
}

/*
* This function is used to get the contents of the directory in the client.
*/
void ldir()
{
	
	DIR *dir;
	struct dirent *direntdir;
	char listing[MAX_SEND_SIZE];
	char currentdir[20] = ".";
	
	dir = opendir(currentdir);
	
	while((direntdir = readdir(dir)) != NULL){
		listing[0] = '\0';
		strcpy(listing, direntdir->d_name);
		
		printf("%s \n",listing);
	}
	closedir(dir);
	
}

/*
*	This function is used to change directory in the server.
* 	The char array passed in holds the name of the directory to change to.
*	socket is the socket used in the communication.
*/
void cd(int socket,char string[], int SIZE)
{
	int service_command,service_status, dir_status;
	char dir[MAX_SEND_SIZE];
	char *dirname;
	
	service_command = SERVERCD;
	service_command = htons(service_command);
	
	if ((write(socket, (char *) &service_command, sizeof(service_command))) <= 0 ){
		printf("Client: Can't write service to server.\n");
	}
	/* Get server response if service is supported */
	if ( read(socket, (char *) &service_status, sizeof(service_status)) <= 0 ){
		printf("Client: can't read server service response.\n");
	}
	service_status = ntohs(service_status);
	if ( service_status == SERVICESUPPORTED ){
		printf("Client: service is supported.\n");
	}
	else if ( service_status == SERVICENOTSUPPORTED ){
		printf("Client: service is not supported.\n");
	}
	
	/* Send directory name to server */
	dirname = splitGetCmd(string,SIZE,2);
	
	strcpy(dir,dirname);
	if (writen (socket, dir, MAX_SEND_SIZE) < MAX_SEND_SIZE){
		printf("Client:error writing directory name to server.\n");
	}
	
	/* Get status if directory is available */
	dir_status = 0;
	if (read (socket, (char *) &dir_status, sizeof(dir_status)) <= 0 ){
		printf("Client: error reading directory status.\n");
	}
	dir_status = ntohs(dir_status);
	if(dir_status == GOODDIR){
		printf("Client: server directory has been changed. \n");
	}
	else if(dir_status == NODIRECTORY){
		printf("Client: no directory with the name given exists in the server. \n");
	}
}
/*
*The function is used to change the directory in the client.
* The char array passed in holds the name of the directory to change to.
*/
void lcd(char string[], int SIZE)
{
	char *dirname;
	int file_desc, oflags;
	mode_t mode = S_IRUSR | S_IWUSR | S_IXUSR;
	
	oflags = O_RDONLY;
	
	dirname = NULL;
	dirname = splitGetCmd(string,SIZE,2);
	
	if (!directory_exists(dirname)){
		printf("No directory exists with that name.\n");
	}
	else{
		file_desc = open(dirname,oflags);
		
		if (fchdir(file_desc) < 0){
			printf("Cannot change directory.\n");
		}
		close(file_desc);
	}
}

/**
* This function is used to get a file from the server.
* socket is used for communication via TCP
* The char array passed in holds the name of the file to be requested.
*/
void request_file(int socket,char string[], int SIZE){
	
	int service_command, service_status, response, fname_size, fname_size_byte, file_status, start_transfer;
	int file_size, num_blks, num_lst_blk, i, amount_written, end_transfer;
	char client_buf[MAX_SEND_SIZE], fname[MAX_SEND_SIZE];
	char *filename;
	FILE *file;
	
	service_command = GETFILE;
	service_command = htons(service_command);
	
	if ((write(socket, (char *) &service_command, sizeof(service_command))) <= 0 ){
		printf("Client: Can't write service to server.\n");
		exit(1);
	}
	/* Get server response if service is supported */
	if ( read(socket, (char *) &service_status, sizeof(service_status)) <= 0 ){
		printf("Client: can't read server service response.\n");
		exit(1);
	}
	service_status = ntohs(service_status);
	if ( service_status == SERVICESUPPORTED ){
		printf("Client: service is supported.\n");
	}
	else if ( service_status == SERVICENOTSUPPORTED ){
		printf("Client: service is not supported.\n");
		exit(1);
	}
	
	/* Send size of filename to server */
	filename = splitGetCmd(string,SIZE,2);
	
	strcpy(fname, filename);
	
	fname_size = strlen(fname);
	fname_size_byte = htons(fname_size);
	if ( write(socket,(char *) &fname_size_byte, sizeof(fname_size_byte)) <= 0 ){
		printf("Client: error writing filename size to server.\n");
		exit(1);
	}
	/* Wait for ackowledgment of receiving size of filename*/
	response = 0;
	if (read(socket, (char *) &response, sizeof(response)) <= 0){
		printf("Client: error receiving filename size acknowledgement.\n");
		exit(1);
	}
	if (ntohs(response) != ACKN ){
		printf("Client: Did not receive acknowledgment of filename size from server.\n");
		exit(1);
	}
	/* Send filename to server */
	if( writen(socket, fname, fname_size) < fname_size ){
		printf("Client: error writing filename to server.\n");
		exit(1);
	}
	file_status = 0;
	/* Get status of file from the server */
	if ( read(socket, (char *) &file_status, sizeof(file_status)) <= 0){
		printf("Client: error reading file status from client. \n");
		exit(1);
	}
	file_status = ntohs(file_status);
	
	if (file_status == GOODFILE){
		/* Send start transfer command to server */
		printf("The file you have requested is a valid file.\n");
		start_transfer = STARTTRANSFER;
		start_transfer = htons(start_transfer);
		if (write(socket, (char *) &start_transfer, sizeof(start_transfer)) <= 0 ){
			printf("Client:error writing start transfer command to server. \n");
			exit(1);
		}
		/* Open file in the client with the same name */
		if ((file = fopen(fname , "w")) == NULL ){
			printf("Cannot open file for reading.\n");
			exit(1);
		}
	}
	else if (file_status == NOFILE){
		printf("Client: There is no file with the name you gave. \n");
		printf("Thank you for using this service. Bye.\n");
		exit(1);
	}
	
	/* Get the filesize from the server */
	file_size = 0;
	if (read(socket, (char *) &file_size, sizeof(file_size)) <= 0){
		printf("Client:error reading file size from server. \n");
		exit(1);
	}
	/* Send ackn of receivng filesize */
	response = ACKN;
	response = htons(response);
	if (write(socket, (char *) &response, sizeof(response)) <= 0 ){
		printf("Client:error sending ackn of file size. \n");
		exit(1);
	}
	
	/* File transfer begins */
	bzero(client_buf, MAX_SEND_SIZE); 
	printf("File Transfer started. \n");
	int amount_rec = 0;
	while((amount_rec = recv(socket, client_buf, MAX_SEND_SIZE, 0)) > 0) 
	{
		amount_written = fwrite(client_buf, sizeof(char), amount_rec, file);
		if(amount_written < amount_rec)
		{
			printf("Client:File write failed on server.\n");
			exit(1);
		}
		bzero(client_buf, MAX_SEND_SIZE);
		if (amount_rec == 0 || amount_rec != MAX_SEND_SIZE) 
		{
			break;
		}
	}
	
	
	/* Transfer of file has ended */
	end_transfer = ENDTRANSFER;
	end_transfer = htons(end_transfer);
	// Write out to client that the file transfer has ended
	if (write(socket, (char *) &end_transfer, sizeof(end_transfer)) <= 0 ){
		exit(1); // Server: send end transfer error.
	}
	printf("File transfer is complete.\n");
	fclose(file);
}

/**
* This function is used to send a file to the server from the client.
*	socket is used for communication via TCP
*	The char array passed in holds the name of the file to be sent to the server.
*/
void send_file(int socket, char string[], int SIZE)
{
	int service_command, service_status, fname_size, fname_size_byte, file_no_read, response, file_status;
	int start_transfer, file_size;
	char *filename;
	char  file_out_buf[MAX_SEND_SIZE], tmp_fname[MAX_SEND_SIZE];
	char fname[MAX_SEND_SIZE];
	FILE *file;
	
	service_command = SENDFILE;
	service_command = htons(service_command);
	
	
	if ((write(socket, (char *) &service_command, sizeof(service_command))) <= 0 ){
		printf("Client: Can't write service to server.\n");
		exit(1);
	}
	/* Get server response if service is supported */
	if ( read(socket, (char *) &service_status, sizeof(service_status)) <= 0 ){
		printf("Client: can't read server service response.\n");
		exit(1);
	}
	service_status = ntohs(service_status);
	if ( service_status == SERVICESUPPORTED ){
		printf("Client: service is supported.\n");
	}
	else if ( service_status == SERVICENOTSUPPORTED ){
		printf("Client: service is not supported.\n");
		exit(1);
	}
	
	/* check if file exists on client */
	filename = splitGetCmd(string,SIZE,2);
	strcpy(fname, filename);
	fname_size = strlen(fname);
	
	
	if (!file_exists(fname)){
		printf("Client: file doesn't exist. \n");
		exit(1);
	}
	else{
		
		
		
		/* send filename size to server */
		
		fname_size_byte = htons(fname_size);
		if ( write(socket,(char *) &fname_size_byte, sizeof(fname_size_byte)) <= 0 ){
			printf("Client: error writing filename size to server.\n");
			exit(1);
		}
		/* Wait for acknowledgment of receiving size of filename*/
		response = 0;
		if (read(socket, (char *) &response, sizeof(response)) <= 0){
			printf("Client: error receiving filename size acknowledgement.\n");
			exit(1);
		}
		if (ntohs(response) != ACKN ){
			printf("Client: Did not receive acknowledgment of filename size from server.\n");
			exit(1);
		}
		
		/* Send filename to server */
		
		if( writen(socket, fname, fname_size) < fname_size ){
			printf("Client: error writing filename to server.\n");
			exit(1);
		}
		//char file_name[file_name_size]; // create array to store filename
		file_status = 0;
		/* Get status of file from the server */
		if ( read(socket, (char *) &file_status, sizeof(file_status)) <= 0){
			printf("Client: error reading file status from client. \n");
			exit(1);
		}
		file_status = ntohs(file_status);

		if (file_status == GOODFILE){
			/* Send start transfer command to server */
			printf("The file you are sending can be received by the server.\n");
			start_transfer = STARTTRANSFER;
			start_transfer = htons(start_transfer);
			if (write(socket, (char *) &start_transfer, sizeof(start_transfer)) <= 0 ){
				printf("Client:error writing start transfer command to server. \n");
				exit(1);
			}
			/* Open file in the client for reading */
			if ((file = fopen(fname , "r")) == NULL ){
				printf("Client can't open the file \n");
				exit(1);
			}
			
		}
		else if (file_status == NOFILE){
			printf("Client: There is a file with the name you gave existing in the server. \n");
			printf("Thank you for using this service. Bye.\n");
			exit(1);
		}
		
		response = 0;
		/* Get response from client about acknowledgment of starting file transfer */
		if ( read(socket, (char *) &response, sizeof(response) ) <= 0 ){
			exit(1);
		}

		if ( ntohs(response) != ACKN ){
			printf("Client: ackn from server to start transfer not received\n");
			exit(1); 
		}
		
		if( ntohs(start_transfer) == STARTTRANSFER ){
			
			file_size = 0;
			/* Get the size of file to be sent and tell client size to expect. */
			

			fseek(file, 0L, SEEK_END);
			file_size = ftell(file);
			/* Send the file size and wait for acknowledgment from server. */
			file_size = htons(file_size);
			if ( write(socket, (char *) &file_size, sizeof(file_size) ) <= 0){
				printf("Client: error writing file size to server. \n");
				exit(1); // Server: read start transfer request error.
			}
			
			response = 0;
			/* Get response from client about acknowledgment of file_size */
			if ( read(socket, (char *) &response, sizeof(response) ) <= 0 ){
				exit(1); // Server: read start transfer request error.
			}

			if ( ntohs(response) != ACKN ){
				return; // Server: size ackn not received.
			}
			/* Place cursor at the beginning of file */
			fseek(file, 0L, SEEK_SET);


			/* Begin file transfer */
			bzero(file_out_buf, MAX_SEND_SIZE); 
			while((file_no_read = fread(file_out_buf, sizeof(char), MAX_SEND_SIZE, file)) > 0){
				if(send(socket, file_out_buf, file_no_read, 0) < 0)
				{
					printf("ERROR: Failed to send file . (errno = %d)\n",  errno);
					break;
				}
				bzero(file_out_buf, MAX_SEND_SIZE); 

			}

			/* Transfer of file has ended */
			response = 0;
			if ( read(socket,(char *) &response, sizeof(response)) <= 0 ){
				printf("Client: error receiving end of transfer command.\n");
				exit(1);
			}
			response = ntohs(response);
			if (response == ENDTRANSFER ){
				printf("File transfer is complete.\n");
			}
			fclose(file);
		}
	}
	
	
}


int main (int argc, char *argv[]){
	
	int sd, n, nr, nw, args, i = 0;
    char host[60], string[BUFSIZE];
    unsigned short port;
    struct sockaddr_in ser_addr; struct hostent *hp;
	

    /* get server host name and port number */
    if (argc==1) {  /* assume server running on the local host and on default port */
         gethostname(host, sizeof(host));
         port = SERV_TCP_PORT;
    } else if (argc == 2) { /* use the given host name */ 
         strcpy(host, argv[1]);
         port = SERV_TCP_PORT;
    } else { 
        printf("Usage: %s [ <server host name> [ <server listening port> ] ]\n", argv[0]); 
        exit(1); 
    }

   /* get host address, & build a server socket address */
    bzero((char *) &ser_addr, sizeof(ser_addr));
    ser_addr.sin_family = AF_INET;
    ser_addr.sin_port = htons(port);
    if ((hp = gethostbyname(host)) == NULL){
          printf("host %s not found\n", host); exit(1);   
    }
    ser_addr.sin_addr.s_addr = * (u_long *) hp->h_addr;

    /* create TCP socket & connect socket to server address */
    sd = socket(PF_INET, SOCK_STREAM, 0);
    if (connect(sd, (struct sockaddr *) &ser_addr, sizeof(ser_addr))<0) { 
         perror("client connect"); exit(1);
    }
	
	/* Choose which option client wants to use for the ftp service. */	
	while (++i){
		string[0] = '\0';
		args = getOption(string,sizeof(string));
		switch(switchOptions(string, sizeof(string), args)){

			case 'a':
			pwd(sd);
			break;

			case 'b':
			lpwd();
			break;

			case 'c':
			dir(sd);
			break;

			case 'd':
			ldir();
			break;

			case 'e':
			cd(sd,string,strlen(string));
			break;

			case 'f':
			lcd(string,strlen(string));
			break;

			case 'g':
			request_file(sd,string,strlen(string));
			break;

			case 'h':
			send_file(sd,string,strlen(string));
			break;

			case 'q':
			printf("Thank you for using this service.\n");
			close(sd);
			exit(0);
			break;

			default:
			printf("None of the commands match your input.\n");
			break;
		}
		printf("\n\n");
	}

	
	return 0;
}


