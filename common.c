#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include  <dirent.h>
#include "common.h"


bool file_exists(char *filename){
	if (access(filename, F_OK) != -1)
	{
		return true;
	}
	
	return false;
}


bool directory_exists (char *dirname)
{
	DIR *dir;
	char folder[512];
	
	strcpy(folder,dirname);
	dir = opendir(folder);
	
	bool exists = false;
	
	if(dir){
		exists = true;
		closedir(dir);
	}
	
	
	return exists;
}