myftp: myftp.o common.o stream.o
	gcc myftp.o common.o stream.o -o myftp

myftp.o: myftp.c stream.c common.c
	gcc -c myftp.c

common.o: common.c common.h
	gcc -c common.c

stream.o: stream.c stream.h
	gcc -c stream.c

clean: 
	rm *.o